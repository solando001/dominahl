<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['middleware' => 'web'], function () {
		Route::get('/', 'GeneralController@landing');
		Route::get('/about', 'GeneralController@about');
		Route::get('/service', 'GeneralController@service');
		Route::get('/contact', 'GeneralController@contact');
		Route::post('/contact', 'GeneralController@sendcontact');
		Route::get('/enquiry', 'GeneralController@enquiry');
        Route::post('/enquiry', 'GeneralController@sendenquiry');
		Route::get('/web', 'GeneralController@web');
		Route::get('/eportal', 'GeneralController@eportal');



		
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
	});
Route::any('{query}',
    function() { return redirect('/'); })
    ->where('query', '.*');


