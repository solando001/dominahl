<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function landing()
    {
    	return view('landing');
    }

    public function about()
    {
    	return view('about');
    }

    public function service()
    {
    	return view('service');
    }

    public function contact()
    {
    	return view('contact');
    }

    public function enquiry()
    {
        return view('enquiry');
    }

    public function web()
    {
        return view('web');
    }

    public function eportal()
    {
        return view('eportal');
    }

    public function sendcontact(Request $request)
    {

        \Mail::send('email.contact',
            array(
                'name' => $request->input('name'),
                'title' => $request->input('title'),
                'email' => $request->input('email'),
                'message' => $request->input('message')
                //'subject' => $request->get('Contact Message From Dominahl Technologies')

            ), function($message) use ($request)
            {
                $message->from($request->email, 'Contact Message To Dominahl Technologies');
                $message->to('joecolemendy@gmail.com', 'Admin')->subject($request->input('title'));
            });



       return back()->with('message', 'Thanks for contacting us, We will get back to you soon!!');

    }

    public function sendenquiry(Request $request)
    {

        \Mail::send('email.contact',
            array(
                'name' => $request->input('name'),
                'title' => $request->input('title'),
                'email' => $request->input('email'),
                'message' => $request->input('message')
                //'subject' => $request->get('Contact Message From Dominahl Technologies')

            ), function($message) use ($request)
            {
                $message->from($request->email, 'Enquiry Message To Dominahl Technologies');
                $message->to('joecolemendy@gmail.com', 'Admin')->subject($request->input('title'));
            });



        return back()->with('message', 'Thanks for the enquiry, We will get back to you soon!!');
        return($request);
    }
}
