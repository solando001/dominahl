@extends('layouts.master')
@section('title', 'Our Services')
@section('content')

<!-- breadcrumb-area start -->
    <div class="breadcrumb-area section-ptb">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="breadcrumb-title">Service</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Service</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->
    
    <!-- content-wraper start -->
    <div class="content-wraper section-pt section-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/lap.jpg" alt="">
                        </div>
                        <div class="service-content text-center">

                                <h3><a href="{{url('/web')}}">Web Development and Dedicated Hosting</a></h3>
                                {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}


                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/macbook.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Deployment of Online & offline e-test Management System</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/software.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Development of school management module</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}

                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/jobs.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Job Recruitment and Outsourcing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}

                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/dm.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Digital Marketing and Content Writing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}

                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/cv.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">CV Writing and Cover Letter Writing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}

                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
            </div>
        </div>
    </div>
    <!-- content-wraper end -->

@endsection
