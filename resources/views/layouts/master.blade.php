<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">

    <!-- CSS
    ========================= -->

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!-- Fonts CSS -->
    <link rel="stylesheet" href="assets/css/icofont.min.css">
    <link rel="stylesheet" href="assets/css/Pe-icon-7-stroke.css">

    <!-- Plugins CSS -->
    <link rel="stylesheet" href="assets/css/plugins.css">

    <!-- Main Style CSS -->
    <link rel="stylesheet" href="assets/css/style.css">

    <!-- Modernizer JS -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

<!-- Main Wrapper Start -->
<div class="main-wrapper">

    <!-- header-area start -->
    <div class="header-area">

        <!-- header-top start -->
        <div class="header-top pt--20 pb--20">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-3 col-12">
                        <!-- logo start -->
                        <div class="logo">
                            <a href="{{url('/')}}"><img src="assets/images/logo/logon.jpg" alt=""></a>
                        </div>
                        <!-- logo End -->
                    </div>
                    <div class="col-lg-8 col-md-9 col-12">
                        <!-- Contact-information-top Start -->
                        <div class="contact-information-top">

                            <!-- Single-information-top Start -->
{{--                            <div class="single-information-top">--}}
{{--                                <div class="info-icon">--}}
{{--                                    <i class="icofont-headphone-alt-2"></i>--}}
{{--                                </div>--}}
{{--                                <div class="info-content">--}}
{{--                                    <h4>Call Us Now </h4>--}}
{{--                                    <p>+2348120692150</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- Single-information-top End -->--}}

                            <!-- Single-information-top Start -->
                            <div class="single-information-top">
                                <div class="info-icon">
                                    <i class="icofont-clock-time"></i>
                                </div>
                                <div class="info-content">
                                    <h4>Mon - Fri</h4>
                                    <p>9.00 am - 5.00 pm</p>
                                </div>
                            </div>
                            <!-- Single-information-top End -->

                        </div>
                        <!-- Contact-information-top End -->
                    </div>
                </div>
            </div>
        </div>
        <!-- header-top end -->

        <!-- header-bottom-area Start -->
        <div class="header-bottom-area header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="header-menu-wrap border-top-1">
                            <!-- main-menu-area Start -->
                            <div class="main-menu">
                                <nav class="main-navigation">
                                    <ul>
                                        <li><a href="{{url('/')}}">HOME</a>
                                           <!--  <ul class="sub-menu">
                                                <li><a href="index.html">Home Page 1</a></li>
                                                <li><a href="index-2.html">Home Page 2</a></li>
                                                <li><a href="index-box.html">Home Box Layout 1</a></li>
                                                <li><a href="index-2-box.html">Home Box Layout 2</a></li>
                                            </ul> -->
                                        </li>
                                        <li><a href="{{url('/eportal')}}">EPORTAL</a>
                                            <!--  <ul class="sub-menu">
                                                 <li><a href="about-box.html">About Box Page</a></li>
                                             </ul> -->
                                        </li>
                                        <li><a href="{{url('/about')}}">ABOUT US</a>
                                           <!--  <ul class="sub-menu">
                                                <li><a href="about-box.html">About Box Page</a></li>
                                            </ul> -->
                                        </li>
                                        <li><a href="{{url('/service')}}">SERVICES</a>
                                            <!-- <ul class="sub-menu">
                                                <li><a href="services-box.html">Services Box Page</a></li>
                                                <li><a href="single-services.html">Single Services</a></li>
                                                <li><a href="single-services-box.html">Single Services Box</a></li>
                                            </ul> -->
                                        </li>
                                        <!-- <li><a href="team.html">TEAM</a>
                                            <ul class="sub-menu">
                                                <li><a href="team-box.html">Team Box Page</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="blog.html">BLOG</a>
                                            <ul class="sub-menu">
                                                <li><a href="blog.html">Blog Right Sidebar</a></li>
                                                <li><a href="blog-box.html">Blog Box Right Sidebar</a></li>
                                                <li><a href="blog-left-sidebar.html">Blog Left Sidebar</a></li>
                                                <li><a href="blog-left-sidebar-box.html">Blog Box Left Sidebar</a></li>
                                                <li><a href="blog-fullwidth.html">Blog Grid Fullwidth</a></li>
                                                <li><a href="blog-fullwidth-box.html">Blog Box Grid Fullwidth</a></li>
                                                <li><a href="blog-details.html">Blog Details Right Sidebar</a></li>
                                                <li><a href="blog-details-box.html">Blog Box Details Right Sidebar</a></li>
                                                <li><a href="blog-details-left-sidebar.html">Blog Details Left Sidebar</a></li>
                                                <li><a href="blog-details-left-sidebar-box.html">Blog Box Details Left Sidebar</a></li>
                                            </ul>
                                        </li> -->
                                        <li><a href="{{url('/contact')}}">CONTACT</a>
                                            <!-- <ul class="sub-menu">
                                                <li><a href="contact-box.html">Contact Box Page</a></li>
                                            </ul> -->
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- main-menu-area End -->

                            <!-- Make-apoinmant Start -->
                            <div class="make-apoinmant-button">
                                <a href="/enquiry" class="default-btn border-radius">Make Enquiry</a>
                            </div>
                            <!-- Make-apoinmant End -->
                        </div>
                    </div>
                    <div class="col">
                        <!-- mobile-menu start -->
                        <div class="mobile-menu d-block d-lg-none"></div>
                        <!-- mobile-menu end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- header-bottom-area End -->

    </div>
    <!-- header-area end -->



   @yield('content')



    <!-- Your-opinion-area Start  -->
    <!-- <div class="your-opinion-area overly-bg-black opinion-bg section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="your-opinion-wrap">
                        <h2>Your Opinion</h2>
                        <div class="opinion-content-inner">
                            <form action="#">
                                <div class="row">
                                    <div class=" col-lg-6 col-md-6">
                                        <div class="input-box">
                                            <input type="text" placeholder="Your Name" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-box">
                                            <input type="text" placeholder="Your Email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="input-box">
                                            <input type="text" placeholder="Address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-6">
                                        <select aria-required="true" class="form-control selector-time-date">
											<option value="" selected data-default>Date</option>
											<option value="1306">13.09</option>
											<option value="1406">14.07</option>
											<option value="1506">15.09</option>
											<option value="1606">16.05</option>
											<option value="1706">17.05</option>
											<option value="1806">18.07</option>
											<option value="1906">19.08</option>
											<option value="2006">20.04</option>
										</select>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-6">
                                        <select aria-required="true" class="form-control selector-time-date">
											<option value="" selected data-default>Time</option>
											<option value="09:00am">09:00 AM</option>
											<option value="10:00am">10:00 AM</option>
											<option value="11:00am">11:00 AM</option>
											<option value="12:00am">12:00 PM</option>
											<option value="13:00pm">13:00 PM</option>
											<option value="14:00pm">14:00 PM</option>
											<option value="15:00pm">15:00 PM</option>
											<option value="16:00pm">16:00 PM</option>
											<option value="17:00pm">17:00 PM</option>
										</select>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <textarea  placeholder="Message" class="form-control"></textarea>
                                    </div>
                                    <div class="col-12">
                                        <div class="submit-box text-center">
                                            <button class="default-btn border-radius" type="submit">Send Opinion</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Your-opinion-area End  -->





    <!-- Latest News Area Start -->
   <!--  <div class="latest-blog-news section-pt section-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>AIRCON NEWS</h4>
                        <h2>Latest News</h2>
                    </div>
                </div>
            </div>
            <div class="row latest-blog-active">
                <div class="col-lg-4">

                    <div class="single-letest-blog mt--30 mb--30">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="assets/images/blog/1.jpg" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="post-mata">
                                <ul>
                                    <li><a href="#">By  Admin</a></li>
                                    <li>/</li>
                                    <li>06 jun 2018</li>
                                </ul>
                            </div>
                            <h3><a href="blog-details.html">Maintaince & Installation</a></h3>
                            <p>long established fact that a reader will page when looking at its layout. </p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4">

                    <div class="single-letest-blog mt--30 mb--30">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="assets/images/blog/2.jpg" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="post-mata">
                                <ul>
                                    <li><a href="#">By  Admin</a></li>
                                    <li>/</li>
                                    <li>23 jun 2018</li>
                                </ul>
                            </div>
                            <h3><a href="blog-details.html">A/C Installation</a></h3>
                            <p>long established fact that a reader will page when looking at its layout. </p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4">

                    <div class="single-letest-blog mt--30 mb--30">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="assets/images/blog/3.jpg" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="post-mata">
                                <ul>
                                    <li><a href="#">By  Admin</a></li>
                                    <li>/</li>
                                    <li>30 jun 2018</li>
                                </ul>
                            </div>
                            <h3><a href="blog-details.html">Optimization You AIR</a></h3>
                            <p>long established fact that a reader will page when looking at its layout. </p>
                        </div>
                    </div>
                    <!-- single-letest-blog End --
                </div>
                <div class="col-lg-4">
                    <!-- single-letest-blog Start --
                    <div class="single-letest-blog mt--30 mb--30">
                        <div class="blog-image">
                            <a href="blog-details.html"><img src="assets/images/blog/6.jpg" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="post-mata">
                                <ul>
                                    <li><a href="#">By  Admin</a></li>
                                    <li>/</li>
                                    <li>30 jun 2018</li>
                                </ul>
                            </div>
                            <h3><a href="blog-details.html">Cleaning & Optimization</a></h3>
                            <p>long established fact that a reader will page when looking at its layout. </p>
                        </div>
                    </div>
                    <!-- single-letest-blog End --
                </div>
            </div>
        </div>
    </div> -->
    <!-- Latest News Area End -->

    <!-- Footer Area Start -->

    @include('layouts.footer')

    <!-- Footer Area End -->

</div>
<!-- Main Wrapper End -->

<!-- JS
============================================ -->

<!-- jQuery JS -->
<script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
<!-- Popper JS -->
<script src="assets/js/popper.min.js"></script>
<!-- Bootstrap JS -->
<script src="assets/js/bootstrap.min.js"></script>
<!-- Plugins JS -->
<script src="assets/js/plugins.js"></script>
<!-- Ajax Mail -->
<script src="assets/js/ajax-mail.js"></script>
<!-- Main JS -->
<script src="assets/js/main.js"></script>


</body>

</html>
