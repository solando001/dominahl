 <footer class="footer-area">
        <div class="footer-top pt--50 pb--100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="footer-info mt--60">
                            <div class="logo">
                                {{--<a href="#"><img src="assets/images/logo/logo-2.png" alt=""></a>--}}
                            </div>
                            <p>Dominahl Technology, a domain of web application development and solution</p>
                            <ul class="social">
                                <li><a href="#"><i class="icofont-facebook"></i></a></li>
                                <li><a href="#"><i class="icofont-google-plus"></i></a></li>
                                <li><a href="#"><i class="icofont-twitter"></i></a></li>
                                <li><a href="#"><i class="icofont-pinterest"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="footer-info  mt--60">
                            <div class="footer-title">
                                <h3>SERVICES</h3>
                            </div>
                            <ul class="footer-list">
                                <li><a href="{{url('/web')}}">Web Development/Hosting</a></li>
                                <li><a href="{{url('/web')}}">Online/Offline E-Test Application</a></li>
                                <li><a href="{{url('/web')}}">School Management Module</a></li>
                                <li><a href="{{url('/web')}}">Job Recruitment and Outsourcing</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="footer-info  mt--60">
                            <div class="footer-title">
                                <h3>Associated Businesses</h3>
                            </div>
                            <ul class="footer-list">
                                <li><a href="https://www.myhotjbz.com" target="_blank">Myhotjobz</a></li>
                                <li><a href="https://www.e-portal.com.ng" target="_blank">E-Portal</a></li>
                                <li><a href="https://www.ivyhill.e-portal.com.ng" target="_blank">Online Exam for Schools</a></li>
                                {{-- <li><a href="https://www.scholar.com.ng" target="_blank">University Scholarship</a></li> --}}
                            </ul>
                            <!-- <ul class="footer-list-instagram">
                                <li><a href="#"><img src="assets/images/instagram/ins-1.jpg" alt=""></a></li>
                                <li><a href="#"><img src="assets/images/instagram/ins-2.jpg" alt=""></a></li>
                                <li><a href="#"><img src="assets/images/instagram/ins-3.jpg" alt=""></a></li>
                                <li><a href="#"><img src="assets/images/instagram/ins-4.jpg" alt=""></a></li>
                                <li><a href="#"><img src="assets/images/instagram/ins-5.jpg" alt=""></a></li>
                                <li><a href="#"><img src="assets/images/instagram/ins-6.jpg" alt=""></a></li>
                            </ul> -->
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="footer-info  mt--60">
                            <div class="footer-title">
                                <h3>QUICK CONTACT</h3>
                            </div>
                            <ul class="footer-list">
                                <li>1309 Coffen Avenue STE 1200<br> Sheridan, Wyoming 82801, USA.</li>
                                <!-- <li><a href="#">airconserv11@gmail.com</a></li> -->
                                <li><a href="mailto:contact@dominahl.com">contact@dominahl.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-bottom-inner text-center">
                            <p>Copyright &copy; Dominahl Technologies LLC.  <?php echo date("Y"); ?> All Right Reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
