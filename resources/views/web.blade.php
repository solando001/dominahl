@extends('layouts.master')
@section('title', 'Our Services')

    @section('content')

        <!-- breadcrumb-area start -->
        <div class="breadcrumb-area section-ptb">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h2 class="breadcrumb-title">Dominahl Technologies Service</h2>
                        <!-- breadcrumb-list start -->
                        <ul class="breadcrumb-list">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item active">Services</li>
                        </ul>
                        <!-- breadcrumb-list end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- breadcrumb-area end -->


        <!-- content-wraper start -->
        <div class="content-wraper section-ptb">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-3">
                        <!-- Nav tabs -->
                        <ul role="tablist" class="nav flex-column service-details-menu dashboard-list">
                            <li class="active"><a href="#tab-list-1" data-toggle="tab" class="nav-link active">Website Development and Hosting</a></li>
                            <li> <a href="#tab-list-2" data-toggle="tab" class="nav-link">Development of Online and Offline e-test Management System</a></li>
                            <li><a href="#tab-list-3" data-toggle="tab" class="nav-link">Development of school Management Module</a></li>
                            <li><a href="#tab-list-4" data-toggle="tab" class="nav-link">Job Recruitment and Outsourcing</a></li>
                            <li><a href="#tab-list-5" data-toggle="tab" class="nav-link">Digital Marketing and Content Writing</a></li>
                            <li><a href="#tab-list-6" data-toggle="tab" class="nav-link">CV, Cover Letter and Proposal Writing</a></li>
                        </ul>
                    </div>
                    <div class="col-md-12 col-lg-9">
                        <!-- Tab panes -->
                        <div class="tab-content dashboard-content">
                            <div class="tab-pane active" id="tab-list-1">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Website Development and Hosting</h2>
                                                <p>We at Dominahl Technologies building user friendly and creatively designed websites that attracts numerous users to boost businesses
                                                    in Africa and beyond and hosting on the best server is our delight</p>
                                                <p>We provide outstanding web applications to digitally promote your business and establishment</p>
                                                {{--<ul class="service-details-list">--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Established fact that a reader will be distracted</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Will be distracted by the readable content</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> The point of using Lorem Ipsum is that’s true</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                        {{--<div class="col-lg-5">--}}
                                            {{--<div class="service-details-image">--}}
                                                {{--<img src="assets/images/service/service-d-1.png" alt="">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                    {{--<p> long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less--}}
                                        {{--normal distribution of letters be distracted by the readable content of a page when looking at its layout. looking at its layout.</p>--}}

                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-list-2">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Development of Online and Offline e-test Management System</h2>
                                                <p>Computer based tests (CBT) is 21st Century’s mode of examination in developing and developed countries.
                                                    It had minimized examination malpractices, enhanced easy and quick evaluation of candidates/students, provided easy access
                                                    to result and enhanced results’ computation among other outstanding benefits. </p>
                                                <p>Dominahl technologies have been in the front line of providing digital solutions to e-assessments using both
                                                    offline and online modules. We have introduced our easy-to-use, user-friendly and reliable e-assessment software to
                                                    secondary and tertiary institutions across Africa. The software does not only proffer access for conducting termly
                                                    examinations but can also be used to set up continuous assessment tests, common entrance examinations, inter school competitions,
                                                    teachers’ assessment tests and staff recruitment exercise.  </p>
                                                <p>We have deployed online and offline e-test management system that have been used by over 5,000 students across Africa.</p>
                                                <p> Our online assessment CBT question bank, <a href="https://www.e-portal.com.ng" target="_blank"><strong>www.e-portal.com.ng</strong></a>
                                                    have been used by secondary students, UTME Students, Post UTME  Students as well as Job Seekers across Africa.</p>
                                                {{--<ul class="service-details-list">--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Established fact that a reader will be distracted</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Will be distracted by the readable content</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> The point of using Lorem Ipsum is that’s true</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-list-3">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Development of school Management Module</h2>
                                                <p>Effective database management is not negotiable when preparing school result for thousands of students.
                                                    Without a carefully and creatively prepared school management system that manages students’ result,
                                                    class test/quizzes, assignment, project/exhibitions and teachers’ evaluation, this effort will be counterproductive.
                                                    We have thus developed a robust, flexible, online and offline school management system fit for use for pre-primary,
                                                    primary, secondary and tertiary institutions. Click here to access/register to use our CBT Platform <a href="www.e-portal.com.ng"><strong>www.e-portal.com.ng</strong></a> </p>
                                                {{--<p>It is a long established fact that a reader will be distracted by the readable content of a page when lookingnormal </p>--}}
                                                {{--<ul class="service-details-list">--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Established fact that a reader will be distracted</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Will be distracted by the readable content</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> The point of using Lorem Ipsum is that’s true</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-list-4">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Job Recruitment and Outsourcing</h2>
                                                <p>Our Job recruitment and outsourcing platform is one of our subsidiary company hosted as myhotjobz technologies.
                                                    The Educational sector of our Country has achieved a great feat in churning out graduates every year.
                                                    However, repeated researches have revealed that a good number of these graduates are either not employable or
                                                    do not have the necessary skills required by employers. Employers and recruiting agents get more confused when
                                                    they have to choose applicants to invite for interview out of hundreds chunks of submitted applications/Cvs.
                                                </p>
                                                <p>On this premise, we introduced a recruiting platform that does not only attract the best hands from several
                                                    fields of study but also recruit for employers to their taste.
                                                    At Myhotjobz technologies, we attract thousands of qualified job seekers to our job blog website daily.
                                                    We have advertised and recruited for many local, government, international and Non-governmental agencies across the globe.
                                                    To automatically refine applications from hundreds of applicants, we employ and automatic qualification and keyword
                                                    matching/selection module available on our website.
                                                </p>
                                                <p>Our crop of experienced recruiters use both Computer Based Tests/Aptittude (CBT), Written Test/Aptitude assessment
                                                    and Team Interviews to select the best candidates for every job position.  Upon request by employers we can also carry out
                                                    online interviews for employers. We are convinced that we can connect you we qualified graduates that will work assiduously
                                                    with your organization to achieve your prime goals within set time. Click <a href="https://www.myhotjbz.com" target="_blank"><strong>www.myhotjbz.com</strong>  </a>
                                                    to access our Job recruitment and outsourcing website</p>
                                                {{--<ul class="service-details-list">--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Established fact that a reader will be distracted</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Will be distracted by the readable content</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> The point of using Lorem Ipsum is that’s true</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-list-5">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Digital Marketing and Content Writing</h2>
                                                <p> Strategic Advertising is the bedrock of growing businesses. Our experienced digital marketing team
                                                    explores several platforms to get businesses across to target audience at the shortest time and cheapest price.
                                                    Our subsidiary, Myhotjobz.com is also open to free and paid adverts. We are poised to ‘get you noticed’ by bringing
                                                    you ‘on board’ to meet your target audience on our highly interactive and engaging contents. Through our contents,
                                                    your target audiences are reached in their thousands daily. </p>
                                                <p><b>Our range of advert includes:</b></p>
                                                <ul class="service-details-list">
                                                    <li><i class="icofont-tick-boxed"></i> Essay Writing</li>
                                                    <li><i class="icofont-tick-boxed"></i> Guest Publication (as many post as possible)</li>
                                                    <li><i class="icofont-tick-boxed"></i> Banner Placing  <i>You can contact us @ <a href="/contact"><strong>HERE</strong></a> for more information</i></li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab-list-6">
                                <div class="service-details-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="service-details-content-inner">
                                                <h2>Curriculumn Vitae, Cover Letter and Proposal Writing</h2>
                                                <p>A Curriculum Vitae is a Job seeker’s passport to landing his/her dream job. Our team of experienced and
                                                    Professional CV/Cover letter writers carefully prepare outstanding, attractive and marketable graduate,
                                                    experienced and business CVs to thousands of clients. Click here to build your marketable CV/Cover letter.</p>
                                                {{--<p>It is a long established fact that a reader will be distracted by the readable content of a page when lookingnormal </p>--}}
                                                {{--<ul class="service-details-list">--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Established fact that a reader will be distracted</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> Will be distracted by the readable content</li>--}}
                                                    {{--<li><i class="icofont-tick-boxed"></i> The point of using Lorem Ipsum is that’s true</li>--}}
                                                {{--</ul>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wraper end -->



    @endsection