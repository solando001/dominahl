@extends('layouts.master')
@section('title', 'Contact Us')
@section('content')

 <!-- breadcrumb-area start -->
    <div class="breadcrumb-area section-ptb">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="breadcrumb-title">Contact Us</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Contact Us</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->

    <div class="contact-page-map">
        <!-- Google Map Start -->
        <div class="container-fluid p-0">
            <div id="map"></div>
        </div>
        <!-- Google Map End -->
    </div>

    <div class="contact-us-area box-contact">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="contact-us-inner">
                        <div class="row">
                            <div class="col-lg-8">
                                <!-- Contact-form-area Start -->
                                <div class="contact-form-area">
                                    <h3>Send Message</h3>
                                    <!-- contact-form-warp Start -->
                                    @if (session('message'))
                                        <div class="alert alert-success">
                                            {{ session('message') }}
                                        </div>
                                    @endif
                                    <div class="contact-form-warp">
                                        <form action="{{url('/contact')}}" method="POST">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="input-box">
                                                        <input type="text" name="name" placeholder="Your Name*" value="{{ old('name') }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="input-box">
                                                        <input type="email" name="email" placeholder="Mail Address*" value="{{ old('email') }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="input-box">
                                                        <input type="text" name="title" placeholder="Title of Message*" value="{{ old('title') }}" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="input-box">
                                                        <textarea name="message" placeholder="Your Message*" required>{{ old('message') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="contact-submit-btn">
                                                <button type="submit" class="submit-btn default-btn">Send Email</button>
                                                {{--<p class="form-messege"></p>--}}
                                            </div>
                                        </form>
                                    </div>
                                    <!-- contact-form-warp End -->
                                </div>
                                <!-- Contact-form-area End -->
                            </div>
                            <div class="col-lg-3 offset-lg-1">
                                <!-- Contact-info-wrap Start -->
                                <div class="contact-info-wrap">
                                    <!-- single-contact-info start -->
                                    <div class="single-contact-info">
                                        <h3>Location</h3>
                                        <p>1309 Coffen Avenue STE 1200<br> Sheridan, Wyoming 82801, USA.</p>
                                    </div>
                                    <!-- single-contact-info End -->
                                    <!-- single-contact-info start -->
{{--                                    <div class="single-contact-info">--}}
{{--                                        <h3>Phone</h3>--}}
{{--                                        <p><a href="#">+2348120692150</a></p>--}}
{{--                                    </div>--}}
                                    <!-- single-contact-info End -->
                                    <!-- single-contact-info start -->
                                    <div class="single-contact-info">
                                        <h3>E-mail</h3>
                                        <p><a href="#">contact@dominahl.com</a></p>
                                    </div>
                                    <!-- single-contact-info End -->
                                </div>
                                <!-- Contact-info-wrap End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
