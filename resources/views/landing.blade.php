@extends('layouts.master')
@section('title', 'Welcome to Dominahl Technologies')

@section('content')

<!-- Hero Slider start -->
    <div class="hero-slider hero-slider-1">
        <div class="single-slide overly-bg-black" 
        style="background-image: url(assets/images/slider/banner1.jpg)">
            <!-- Hero Content One Start -->
            <div class="hero-content-one container">
                <div class="row">
                    <div class="col-lg-12 "> 
                        <div class="slider-text-info">
                            <h1>We build Efficient<br>  and Reliable Web Applications</h1>
                            <p>Fast and efficient deployment of web applications and development </p>
                            <div class="slider-button">
                                <a href="/about" class="default-btn border-radius">Explore</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Hero Content One End -->
        </div>
    </div>
    <!-- Hero Slider end -->

     <!-- Repair-make-area Start -->
    <div class="repair-make-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="repair-service-inner">
                        <h3>Welcome to Dominahl Technologies</h3>
                        {{--<h2>About Us</h2>--}}
                        <p>Dominahl Technologies is a software outsourcing company focused on developing efficient and reliable system that meet users’ needs. Our core area of specialization is <i><b>education, e-commerce, enterprise software,
                                    job recruitment and outsourcing</b></i>.
                            We also specialize in web solutions, mobile and desktop application development. </p>
                            <p>We are customer friendly and look forward to partnering with you to add value and measure to your business.</p>

                        <div class="make-apoinment-button">
                            <a href="/enquiry" class="default-btn border-radius">Make Enquiry</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="repair-image pt--30">
                        <img src="assets/images/banner/00.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Repair-make-area End -->

     <!-- Our Service Area Start -->
    <div class="our-service-area bg-grey section-pt section-pb-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>---</h4>
                        <h2>Our Services</h2>
                    </div>
                </div>
            </div>
            <div class="row service-slider-active">
                <div class="col-lg-4 col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mt--30 mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/lap.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Web Development & Dedicated Hosting</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>

                <div class="col-lg-4  col-md-6">
                    <!-- single-service Start -->
                    <div class="single-service mt--30 mb--30">
                        <div class="service-image">
                            <img src="assets/images/service/macbook.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Deployment of Online & offline e-test Management System</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
                <div class="col-lg-4  col-md-6 mb--30">
                    <!-- single-service Start -->
                    <div class="single-service mt--30">
                        <div class="service-image">
                            <img src="assets/images/service/software.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Development of school management module</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>

                <div class="col-lg-4  col-md-6 mb--30">
                    <!-- single-service Start -->
                    <div class="single-service mt--30">
                        <div class="service-image">
                            <img src="assets/images/service/jobs.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Job Recruitment and Outsourcing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>

                <div class="col-lg-4  col-md-6 mb--30">
                    <!-- single-service Start -->
                    <div class="single-service mt--30">
                        <div class="service-image">
                            <img src="assets/images/service/dm.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">Digital Marketing and Content Writing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>

                <div class="col-lg-4  col-md-6 mb--30">
                    <!-- single-service Start -->
                    <div class="single-service mt--30">
                        <div class="service-image">
                            <img src="assets/images/service/cv.jpg" alt="">
                        </div>
                        <div class="service-content text-center">
                            <h3><a href="{{url('/web')}}">CV and Cover Letter Writing</a></h3>
                            {{--<p>Various versions have evolved over the years, sometimes by accident,</p>--}}
                            {{--<a href="">READ MORE</a>--}}
                        </div>
                    </div>
                    <!-- single-service End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Our Service Area End -->
    
   

    <!-- Project-count-inner Start -->
    <div class="project-count-inner section-pb section-pt-80">
        <div class="container">
            <div class="row">
            <div class="col-lg-3 col-sm-6">
                <!-- counter start -->
                <div class="counter text-center">
                    <h3 class="counter-active">5045</h3>
                    <p>Registered Users</p>
                </div>
                <!-- counter end -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- counter start -->
                <div class="counter text-center">
                    <h3 class="counter-active">9</h3>
                    <p>Project Done</p>
                </div>
                <!-- counter end -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- counter start --> 
                <div class="counter text-center">
                    <h3 class="counter-active">3835</h3>
                    <p>Satisfied Customers</p>
                </div>
                <!-- counter end -->
            </div>
            <div class="col-lg-3 col-sm-6">
                <!-- counter start -->
                <div class="counter text-center">
                    <h3 class="counter-active">3</h3>
                    <p>Running Project</p>
                </div>
                <!-- counter start -->
            </div>
        </div>
        </div>
    </div>
    <!-- Project-count-inner End -->

     <!-- FAQ area Start -->
    <div class="faq-area bg-grey section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h4>---</h4>
                        <h2>Testimony</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <!-- single-faq Start -->
                    {{--<div class="single-faq text-center">--}}
                        {{--<div class="title-content">--}}
                            {{--<h4>Can i fix my air conditioner prb in  my home anyway</h4>--}}
                            {{--<i class="icofont-question"></i>--}}
                        {{--</div>--}}
                        {{--<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>--}}
                    {{--</div>--}}
                    <!-- single-faq Start -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- single-faq Start -->
                    <div class="single-faq text-center">
                        <div class="title-content">
                            <h4>Our students have enjoyed their experience with CBT examinations conducted termly with e-portal software deployed by Dominahl Technologies</h4>
                            {{--<i class="icofont-question"></i>--}}
                            <i></i>
                        </div>
                        <p>Principal Ivyhill Academy, Ilorin </p>
                    </div>
                    <!-- single-faq Start -->
                </div>
                <div class="col-lg-4 col-md-6">
                    <!-- single-faq Start -->
                    {{--<div class="single-faq text-center">--}}
                        {{--<div class="title-content">--}}
                            {{--<h4>Can i fix my air conditioner prb in  my home anyway</h4>--}}
                            {{--<i class="icofont-question"></i>--}}
                        {{--</div>--}}
                        {{--<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>--}}
                    {{--</div>--}}
                    <!-- single-faq Start -->
                </div>
            </div>
        </div>
    </div>
    <!-- FAQ area End -->

    <!-- Something New Aera Start -->
    <div class="something-new-area something-new-bg overly-bg-black section-ptb">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="something-new-inner text-center text-white">
                        <h2>Dominahl Technology</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
                        <div class="contact-us-button">
                            <a href="/service" class="default-btn border-radius">Know More</a> 
                            <a href="/contact" class="primary-btn border-radius">Contact Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Something New Aera End -->

@endsection