@extends('layouts.master')
@section('title', 'EPortal | Computer Based Test Module')

@section('content')


    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area section-ptb">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="breadcrumb-title">EPortal.com.ng</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Eportal.com.ng</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->

    <!-- Repair-make-area Start -->
    <div class="repair-make-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12 col-md-12">
                    <div class="repair-service-inner">
                        <br>
                        <h3>Computer Based Test Module</h3>
                        <h2>Eportal.com.ng</h2>
                        <p>
                            Our efficient and user friendly online and offline CBT assessment platform is a rare combination of educational
                            and artificial intelligence technology. The CBT platform can be used in primary schools, colleges and tertiary
                            institutions as well as industries (for interview assessments)
                            to conduct theory and multi-choice (objective) assessment.
                        </p>

                        <p>
                            The Paradigm shift in information technology has aroused the need to digitalize
                            the assessment mode in the Educational Sector. Computer based assessment (CBT) has proved to be the
                            lasting and effective solution for mass education evaluation. The paper and pen (manual) method of writing examination,
                            which has been in existence for decades, may not be appealing for use anymore because of the problems usually experienced
                            including examination malpractices, examination venue capacity constraints, delay in the release of results,
                            cost implication of printing examination materials and human error. Thus, there is the need for automation of the examination system.
                        </p>

                        <p>
                            In order to proactively overcome these challenges, our National examination bodies in Nigeria have not only deployed a
                            computerized mode of marking but also adopted e-assessment platform especially the Joint Admissions and Matriculation Examination
                            Board (JAMB) and the Federal Government Scholarships Board (FSB). Notably, 95% of Post UTME Examinations in Nigeria are now conducted
                            using CBT. It is believed that other Examination Bodies directly involved in conducting examinations
                            in the secondary schools will soon deploy Computer based tests. Hence you need to make your termly examinations computer based.

                            <br>
                            Dominahl technologies have been in the front line of providing digital solutions to e-assessments using both offline and online modules.
                            We are pleased to introduce our easy-to-use, user-friendly and reliable e-assessment software to you. The software does not only
                            proffer access for conducting termly examinations
                            but can also be used to set up continuous assessment tests, inter school competitions and staff recruitment exercise.
                        </p>
                        <div class="make-apoinment-button">
                            {{--<a href="/enquiry" class="default-btn border-radius">Make Enquiry</a>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="repair-image pt--30">
                        <img src="assets/images/Picture1.png" alt="">
                    </div>
                    <br><br><br><br>
                    <p>Typical Home Page of EPortal.com.ng</p>
                    <br>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="repair-image pt--30">
                        <img src="assets/images/Picture4.png" alt="">
                    </div>
                    <br>
                    <p>Typical Exam Page (Multiple choice Question)</p>
                    <br>
                </div>

                <div class="col-lg-12 col-md-12">
                    <div class="repair-service-inner">
                        <h3>Pricing</h3>
                        <p>
                            We charge based on the number of students registered on the portal. Kindly contact our help desk through
                            (details below) to confirm the pricing for your institution. These charges can be integrated into the school fee
                            breakdown as ‘CBT/ICT FEE’. Schools with over 100 registered students are given 10% discount.
                            All users are given free access for the first term/semester.
                        </p>
                        <br>

                        <h3>Deployment</h3>
                        <p>
                            Our team of developer will deploy the app on the school server, either online or offline. We are committed to giving
                            the best service at an affordable price.
                            We anticipate to bring experienced digital solutions currently used by over 3500 subscribers online and offline to your school
                            so as to contribute to your vision
                            of building and training critical thinkers to reposition our country among the great Nations of the World.
                        </p>

                        <br>
                        <h3>Student-Teacher Interactive Platform</h3>
                        <p>
                            Our e-portal learning and interactive platform useful for virtual learning especially when learners and tutors cannot be
                            physically present in the same learning centre. This remote learning web app affords the opportunity for teachers and students to
                            communicate effectively. Virtual learning platforms like this can be used during the COVID 19 Pandemic
                            that has occassioned the closure of schools and colleges. Looking for Free Virtual learning platform? Use e-portal
                        </p>

                        <br>
                        <h3>Features of the Platform</h3>
                        <p>
                           <li>
                            The platform is deployed online on a unique subdomain for your institution. (
                            for example: schoolname.e-portal.com.ng)
                            </li>
                            <li>
                                Teachers can post lesson notes, video of teacher (while teaching), audio of teacher (while teaching),
                                assignments (which will be automatically graded) and pictures (images) of the lesson.
                            </li>

                        <li>
                            Admin officers or school head (or teacher) can decide to publish or unpublish uploaded lessons
                        </li>

                        <li>
                            Teachers can upload <b>theory or objective questions</b> for the students. The objective questions are CBT.
                            <b>THE PORTAL CAN BE USED FOR TERMLY EXAMS OR C.A TESTS</b>
                        </li>

                        <li>
                            Students can take lessons from home by logging in to the unique portal of the school,
                            do assignments and reply the teacher.
                        </li>

                        <li>
                            Students can send pictures of assignments done back to the teacher
                        </li>

                        <li>
                            Students can view results immediately except during exam sessions
                        </li>

                        <li>
                            Students can copy lesson notes from the portal
                        </li>

                        <li>
                            The portal is personalized and secured for the school
                        </li>

                        <li>
                            The school can set up the CBT platform using their computer room.
                            This will upgrade the status of the school as an IT Complaint school
                        </li>

                        <li>
                            The portal is free for the first term (or semester) of usage as
                            CBT PLATFORM and free for the first 50 students using it during covid 19 lock down break.
                        </li>


                        </p>

                        <p>
                            <b>
                                <i>For Enquiries please contact: +2349028583990, +2348137331282
                                <br>
                                Email: dominahltech@gmail.com
                                </i>
                            </b>
                        </p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Repair-make-area End -->

@endsection