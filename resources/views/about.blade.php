@extends('layouts.master')
@section('title', 'About Us')
@section('content')


    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area section-ptb">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="breadcrumb-title">About Us</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">About Us</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->
    
    <!-- Repair-make-area Start -->
    <div class="repair-make-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="repair-service-inner">
                        <h3>--</h3>
                        <h2>About Us</h2>
                        <p>Dominahl Technologies is a leading global provider of web solutions for students, job seekers and fast growing businesses in Africa and beyond. </p>
                        <p>Over the years, we have been in the front line of providing digital solutions to e-assessments using both offline and online modules for students,
                            career development and job recruitment platforms for graduates and job seekers, as well as web solutions to drive the growth of businesses in Africa and beyond..</p></p>
                        <div class="make-apoinment-button">
                            <a href="/enquiry" class="default-btn border-radius">Make Enquiry</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="repair-image pt--30">
                        <img src="assets/images/banner/about.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Repair-make-area End -->
    
    <!-- Project-count-inner Start -->
    <div class="project-count-inner section-pb section-pt-80">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <!-- counter start -->
                    <div class="counter text-center">
                        <h3 class="counter-active">5045</h3>
                        <p>Total User</p>
                    </div>
                    <!-- counter end -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <!-- counter start -->
                    <div class="counter text-center">
                        <h3 class="counter-active">6</h3>
                        <p>Project Done</p>
                    </div>
                    <!-- counter end -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <!-- counter start -->
                    <div class="counter text-center">
                        <h3 class="counter-active">3835</h3>
                        <p>Satisfied Customers</p>
                    </div>
                    <!-- counter end -->
                </div>
                <div class="col-lg-3 col-sm-6">
                    <!-- counter start -->
                    <div class="counter text-center">
                        <h3 class="counter-active">3</h3>
                        <p>Running Project</p>
                    </div>
                    <!-- counter start -->
                </div>
            </div>
        </div>
    </div>
    <!-- Project-count-inner End -->

@endsection